import logging
from typing import Tuple, List

from PIL import Image, ImageDraw
from PIL.ImageDraw import Draw

from data import XPM_LIST
from xpms.processor import process_term_xpm


class XPMToPNG:
    _data_list: List[str] = XPM_LIST
    image_size: Tuple[int, int]
    _height: int
    _width: int
    image_mode: str = "RGBA"
    image_background: str = "white"
    image_foreground: str = "black"
    _image: Image
    filename_to_save: str
    _draw: Draw

    def __init__(self, data_list: List[str], filename: str, **kwargs):
        self._data_list = data_list
        self.filename_to_save = filename
        self._height = len(self._data_list)
        self._width = len(self._data_list[0])
        self.image_size = (self._width, self._height)
        self.image_mode = kwargs.get("image_mode", self.image_mode)
        self.image_color = kwargs.get("image_color", "white")

        self._image = Image.new(mode=self.image_mode, size=self.image_size, color=self.image_color)
        self._draw = ImageDraw.Draw(self._image)

    def scale_up(self, times: int = 5):
        new_data_list = []
        for hl in self._data_list:
            parts = []
            hl_length = len(hl)
            curr_index = 1
            end_index = hl_length

            temp = hl[0]
            while curr_index < end_index:
                if hl[curr_index] == temp[-1]:
                    temp += hl[curr_index]
                else:
                    parts.append(temp)
                    temp = hl[curr_index]
                curr_index += 1
            parts.append(temp)
            parts = list(map(lambda p: p * times, parts))
            joined_parts = "".join(parts)
            for _ in range(times):
                new_data_list.append(joined_parts)

        self._data_list = new_data_list
        self._height = len(self._data_list)
        self._width = len(self._data_list[0])
        self.image_size = (self._width, self._height)

        self._image = Image.new(mode=self.image_mode, size=self.image_size, color=self.image_color)
        self._draw = ImageDraw.Draw(self._image)

    def draw_horizontal_line(self, line_data: str, row: int):
        column: int = 0
        for char in line_data:
            if char == "F":
                fill_color = self.image_foreground
                self._draw.line((column, row) + (column, row), fill=fill_color)
            column += 1

    def draw_image(self):
        for row, horizontal_line in enumerate(self._data_list):
            # logging.info(f"Writing row {row}")
            self.draw_horizontal_line(line_data=horizontal_line, row=row)

    def save(self):
        self._image.save(self.filename_to_save)


if __name__ == '__main__':
    logging.basicConfig(format="%(levelname)s [%(asctime)s] - %(message)s", level=logging.DEBUG)
    im = XPMToPNG(data_list=process_term_xpm(), filename="terminator.png")
    im.scale_up(times=5)
    im.draw_image()
    im.save()